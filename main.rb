# CONNECT 4
NUMBER_OF_ROWS = 6
NUMBER_OF_COLUMNS = 7
PLAYER_ONE_CHECKER = "x"
PLAYER_TWO_CHECKER = "o"

$game_board = Array.new(NUMBER_OF_ROWS) { Array.new(NUMBER_OF_COLUMNS) { 0 } }
$is_player_one_turn = true
$is_game_finished = false

# colorize text
class String
    def black;          "\e[30m#{self}\e[0m" end
    def red;            "\e[31m#{self}\e[0m" end
    def green;          "\e[32m#{self}\e[0m" end
    def brown;          "\e[33m#{self}\e[0m" end
    def blue;           "\e[34m#{self}\e[0m" end
    def magenta;        "\e[35m#{self}\e[0m" end
    def cyan;           "\e[36m#{self}\e[0m" end
    def gray;           "\e[37m#{self}\e[0m" end
end

# waits for player input
def wait_for_player
    puts "Choose column (1 - 7)"
    player_input = gets
    return player_input.to_i
end

# gets the first empty cell within the chosen column
def get_first_empty_cell(column)
    i = NUMBER_OF_ROWS - 1

    while i >= 0
        if $game_board[i][column] == 0
            return i
        end
        i -= 1
    end

    return -1
end

# place the checker in the cell 
def place_checker(column)
    # we subtract 1 for getting the right number since the game board array goes from 0 to 6
    column -= 1

    # error handling
    if column < 0 or column >= NUMBER_OF_COLUMNS
        puts "The number of columns must be between 1 and 7"
        return -1
    end

    empty_cell = get_first_empty_cell(column)
    if empty_cell == -1
        puts "The column is full"
        return -1
    else
        # place the corresponding checker
        checker = $is_player_one_turn ? PLAYER_ONE_CHECKER : PLAYER_TWO_CHECKER
        $game_board[empty_cell][column] = checker
    end

    return 0
end

# prints the game board
def print_board
    puts

    for row in $game_board
        for cell in row
            # check whether to use a color or not, depending on the cell value
            if cell == PLAYER_ONE_CHECKER
                print cell.cyan
            elsif cell == PLAYER_TWO_CHECKER 
                print cell.red
            else
                print cell
            end
            print " "
        end
    end

    puts
end

# check different combinations for players to win
def check_winner
    checker = $is_player_one_turn ? PLAYER_ONE_CHECKER : PLAYER_TWO_CHECKER

    # check horizontal
    for y in  0..NUMBER_OF_ROWS-1
        for x in 0..NUMBER_OF_COLUMNS-4
            if $game_board[y][x] == checker and $game_board[y][x + 1] == checker and $game_board[y][x + 2] == checker and $game_board[y][x + 3] == checker
                return true
            end
        end
    end

    # check vertical
    for y in  0..NUMBER_OF_ROWS-4
        for x in 0..NUMBER_OF_COLUMNS-1
            if $game_board[y][x] == checker and $game_board[y + 1][x] == checker and $game_board[y + 2][x] == checker and $game_board[y + 3][x] == checker
                return true
            end
        end
    end

    # check positive diagonal
    for y in  0..NUMBER_OF_ROWS-4
        for x in 0..NUMBER_OF_COLUMNS-4
            if $game_board[y][x] == checker and $game_board[y + 1][x + 1] == checker and $game_board[y + 2][x + 2] == checker and $game_board[y + 3][x + 3] == checker
                return true
            end
        end
    end

    # check negative diagonal
    for y in  3..NUMBER_OF_ROWS-1
        for x in 0..NUMBER_OF_COLUMNS-4
            if $game_board[y][x] == checker and $game_board[y - 1][x + 1] == checker and $game_board[y - 2][x + 2] == checker and $game_board[y - 3][x + 3] == checker
                return true
            end
        end
    end

    return false
end

# game loop
def game 
    while !@is_game_finished
        player = $is_player_one_turn ?  "Player One" : "Player Two"
        puts "#{player} turn"

        # loop to keep asking a player for a valid input
        while place_checker(wait_for_player()) == -1
        end

        print_board()

        # if there is a winner we print it and exit the game loop
        if check_winner()
            puts
            puts "####################"
            puts "#{player} has won!".gray
            puts "####################"
            puts
            break
        end

        # change turn
        $is_player_one_turn = !$is_player_one_turn
        puts
        puts "=================================================="
        puts
    end

    # prevent window from closing
    gets
end

game()